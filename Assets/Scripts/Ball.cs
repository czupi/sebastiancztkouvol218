﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public Rigidbody2D rb;
    private bool isClicked = false;
    public bool isGrounded = false;
    public GameObject prefab;
    float releaseTime = .2f;
    public float groundDistance;
    public LayerMask groundMask;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(prefab.transform.position, groundDistance, groundMask);

        if (isGrounded)
        {
            Instantiate(prefab);
            this.GetComponent<SpringJoint2D>().enabled = true;
        }

        if (isClicked)
        {

            rb.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    }

    private void OnMouseDown()
    {
        isClicked = true;
        rb.isKinematic = true;
    }
    private void OnMouseUp()
    {
        isClicked = false;
        rb.isKinematic = false;
        StartCoroutine(Release());
    }

    IEnumerator Release()
    {
        yield return new WaitForSeconds(releaseTime);

        GetComponent<SpringJoint2D>().enabled = false;
    }
}
